<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://github.com/skarampatakis
 * @since             1.0.0
 * @package           Skos_Taxonomy_Plugin
 *
 * @wordpress-plugin
 * Plugin Name:       SKOS Taxonomy
 * Plugin URI:        http://okfn.gr
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Sotirios Karampatakis
 * Author URI:        http://github.com/skarampatakis
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       skos-taxonomy-plugin
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

define( 'SKOS_TAXONOMY_PLUGIN_PREFIX', 'SKOSTP' );

define( 'SKOS_TAXONOMY_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-skos-taxonomy-plugin-activator.php
 */
function activate_skos_taxonomy_plugin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-skos-taxonomy-plugin-activator.php';
	Skos_Taxonomy_Plugin_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-skos-taxonomy-plugin-deactivator.php
 */
function deactivate_skos_taxonomy_plugin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-skos-taxonomy-plugin-deactivator.php';
	Skos_Taxonomy_Plugin_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_skos_taxonomy_plugin' );
register_deactivation_hook( __FILE__, 'deactivate_skos_taxonomy_plugin' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-skos-taxonomy-plugin.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_skos_taxonomy_plugin() {

	$plugin = new Skos_Taxonomy_Plugin();
	$plugin->run();

}
run_skos_taxonomy_plugin();

if ( defined( 'WP_CLI' ) && WP_CLI ) {
function skos_import( $args ) {
       $taxonomy = "fields-of-science";
       $test = new Skos_Taxonomy_Plugin_Functions();
       $test->tree_cli($taxonomy);

    WP_CLI::success( $args[0] );
}
WP_CLI::add_command( 'skos-import', 'skos_import' );
}
