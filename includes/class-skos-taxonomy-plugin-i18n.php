<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://github.com/skarampatakis
 * @since      1.0.0
 *
 * @package    Skos_Taxonomy_Plugin
 * @subpackage Skos_Taxonomy_Plugin/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Skos_Taxonomy_Plugin
 * @subpackage Skos_Taxonomy_Plugin/includes
 * @author     Sotirios Karampatakis <s.karampatakis@gmail.com>
 */
class Skos_Taxonomy_Plugin_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'skos-taxonomy-plugin',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
