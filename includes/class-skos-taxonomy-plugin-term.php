<?php
class Skos_Taxonomy_Plugin_Term{

  protected $meta_key_iri = SKOS_TAXONOMY_PLUGIN_PREFIX . "_IRI";

  protected $meta_key_lang = SKOS_TAXONOMY_PLUGIN_PREFIX . "_LANG";

  public function __construct(\EasyRdf_Resource $resource, $taxonomy, $parent_id, $language){
    $this->resource = $resource;
    $this->taxonomy = $taxonomy;
    $this->language = $language;
    $this->iri = $resource->getUri();;
    $this->parent_id = $parent_id;
    $this->label = $this->label();
    $this->slug = $this->slug();

  }

  public function insert_term(){
    $temp_term =  wp_insert_term(
          $this->label,
          $this->taxonomy,
          [
            'parent' => $this->parent_id,
            'slug' => $this->slug
          ]
        );
    $term_id = $temp_term["term_id"];
    //TODO: check if term is needed otherwise remove it and return term id instead
    $term = get_term($term_id);
    $this->add_meta_values($term_id);
    return $term;
  }

  public function update_term($id){
    $temp_term = wp_update_term(
      $id,
      $this->taxonomy,
      [
        "name" => $this->label,
        "parent" => $this->parent_id,
        "slug" => $this->slug
      ]
    );

    $term_id = $temp_term["term_id"];
    //TODO: check if term is needed otherwise remove it and return term id instead
    $term = get_term($term_id);
    $this->add_meta_values($term_id);
    return $term;
  }

  public function add_meta_values($id){
    $this->add_iri_meta($id);
    $this->add_lang_meta($id);
  }

  public function label(){
    return $this->resource->label($this->language) != null ?  $this->resource->label($this->language)->getValue() : $this->resource->label("en")->getValue();
  }

  public function slug(){
    return sanitize_title($this->label . " " . $this->language );
  }

  public function add_iri_meta($id){
    add_term_meta($id, $this->meta_key_iri, $this->iri, true);
  }

  public function add_lang_meta($id) {
    add_term_meta($id, $this->meta_key_lang, $this->language, true);
    pll_set_term_language($id, $this->language);
  }

}
