<?php

/**
 * Fired during plugin activation
 *
 * @link       http://github.com/skarampatakis
 * @since      1.0.0
 *
 * @package    Skos_Taxonomy_Plugin
 * @subpackage Skos_Taxonomy_Plugin/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Skos_Taxonomy_Plugin
 * @subpackage Skos_Taxonomy_Plugin/includes
 * @author     Sotirios Karampatakis <s.karampatakis@gmail.com>
 */
class Skos_Taxonomy_Plugin_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		
	}

}
