<?php
class Skos_Taxonomy_Plugin_Functions {

  protected $firstLevelPath = "skos:hasTopConcept";

  protected $inverseFirstLevelPath = "^skos:topConceptOf";

  protected $secondLevelPath = "skos:narrower";

  protected $inverseSecondLevelPath = "^skos:broader";

  protected $meta_key_iri = SKOS_TAXONOMY_PLUGIN_PREFIX . "_IRI";

  protected $meta_key_lang = SKOS_TAXONOMY_PLUGIN_PREFIX . "_LANG";

  public function __construct(){
    $this->graph = $this->graph();
    $this->scheme = $this->scheme();
  }

  public function scheme() {
    return ($this->graph->allOfType("skos:ConceptScheme"))[0];
  }

  public function concepts() {
    return $this->graph->allOfType("skos:Concept");
  }

  public function graph(){
    $skos = new EasyRdf_Graph();
    $skos->parseFile(SKOS_TAXONOMY_PLUGIN_PATH . "phdhub_core_graph.rdf");
    return $skos;
  }

  public function public_custom_taxonomies(){
    $args = array(
  		'public'   => true,
  		'_builtin' => false
  	);

  	$output = 'names'; // or objects
  	$operator = 'and'; // 'and' or 'or'
  	$taxonomies = get_taxonomies( $args, $output, $operator );

    return $taxonomies;
  }

  public function tree($taxonomy) {
    ini_set("max_execution_time", 300);
    ini_set("memory_limit", "512M");
    $graph = $this->graph;
    $parent = $this->scheme;
    $languages = pll_languages_list();
    foreach ($languages as $language){
      $parent_term = $this->add_or_update_term($parent, $taxonomy, $language);
      $children = $this->find_children($graph, $this->firstLevelPath, $parent, $taxonomy, $parent_term, $language);
      if (count($children) == 0) {
        $children = $this->find_children($graph, $this->inverseFirstLevelPath, $parent, $taxonomy, $parent_term, $language);
      }
    }
    if($languages){
	echo "Tree walk finished...";
    	$this->relate_translations($taxonomy);
    	echo "Translation relations finished";
    }
    else{
    	echo "No languages selected on the Polylang plugin. Please first add some languages";
    }
    return;
  }

  public function tree_cli($taxonomy) {
    ini_set("max_execution_time", 300);
    ini_set("memory_limit", "512M");
    $graph = $this->graph;
    $parent = $this->scheme;
    $resources = $graph->allOfType("skos:Concept");
    $languages = pll_languages_list();
    foreach ($languages as $language){
      WP_CLI::log( "Adding language" . $language);
      $progress = \WP_CLI\Utils\make_progress_bar(' Importing SKOS Taxonomy Terms', count($resources)+1 );
      $parent_term = $this->add_or_update_term($parent, $taxonomy, $language);
      $progress->tick();
      $children = $this->find_children($graph, $this->firstLevelPath, $parent, $taxonomy, $parent_term, $language, $progress);
      if (count($children) == 0) {
        $children = $this->find_children($graph, $this->inverseFirstLevelPath, $parent, $taxonomy, $parent_term, $language, $progress);
      }
      $progress->finish();
    }
    if($languages){
        WP_CLI::log ("Tree walk finished...");
        $this->relate_translations($taxonomy, true);
        WP_CLI::log("Translation relations finished");
    }
    else{
        echo "No languages selected on the Polylang plugin. Please first add some languages";
    }
    return;
  }


  public function find_children(\EasyRdf_Graph $graph, $hierarchic_link, $parent_url, $taxonomy, $parent_term, $language, $progress = null) {
      $children = $graph->allResources($parent_url, $hierarchic_link);
      $myJSON = [];
      foreach ($children as $child) {
          $term = $this->add_or_update_term($child, $taxonomy, $language, $parent_term, $progress);
          $children = $this->find_children($graph, $this->secondLevelPath, $child, $taxonomy, $term, $language, $progress);
          if (count($children) == 0) {
              $children = $this->find_children($graph, $this->inverseSecondLevelPath, $child, $taxonomy, $term, $language, $progress);
          }
      }
      return $children;
  }

  public function add_or_update_term(\EasyRdf_Resource $resource, $taxonomy, $language, $parent = 0, $progress = null) {
    $parent_id = $parent === 0 ? 0 : $parent->term_id;
    $check_term = $this->get_term_by_iri_and_lang($resource->getUri(), $language, $taxonomy);
    $skos_term = new Skos_Taxonomy_Plugin_Term($resource, $taxonomy, $parent_id, $language);
    if(!$check_term){
      $term = $skos_term->insert_term();
    }
    else{
      $term = $skos_term->update_term($check_term->term_id);
    }
    if($progress){
        $progress->tick();
    }
    return $term;
  }

  public function get_term_by_iri_and_lang($iri, $lang, $taxonomy){
    $args = array(
        'hide_empty' => false, // also retrieve terms which are not used yet
        'taxonomy' => $taxonomy,
        'meta_query' => [
          [
            'key' => $this->meta_key_iri,
            'value' => $iri,
            'compare' => '='
            ],
          [
            'key' => $this->meta_key_lang,
            'value' => $lang,
            'compare' => '='
            ]
          ],
        'number' => 1
    );
    $terms = get_terms( $args );
    return !empty($terms) ? $terms[0] : false;
  }

  public function relate_translations ($taxonomy, $cli = false) {
    $array = $this->concepts();
    array_push($array, $this->scheme);
    if($cli){
        $progress = \WP_CLI\Utils\make_progress_bar(' Relating translations', count($array) );
    }
    foreach( $array as $concept){
      $terms = $this->get_terms_by_iri($concept->getUri(), $taxonomy);
      if($terms) {
        $array = $this->create_associative_array($terms);
        pll_save_term_translations($array);
      }
      if($cli){
          $progress->tick();
      }
    }
    if($cli){
        $progress->finish();
    }
  }

  public function create_associative_array ($terms) {
    $array = [];
    foreach($terms as $term){
      $array[pll_get_term_language($term->term_id)] = $term->term_id;
    }
    return $array;
  }

  public function get_terms_by_iri ($iri, $taxonomy){
    $args = array(
        'hide_empty' => false, // also retrieve terms which are not used yet
        'taxonomy' => $taxonomy,
        'meta_query' => [
          [
            'key' => $this->meta_key_iri,
            'value' => $iri,
            'compare' => '='
          ]
        ]
    );
    $terms = get_terms( $args );
    return !empty($terms) ? $terms : false;
  }
}
