<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://github.com/skarampatakis
 * @since      1.0.0
 *
 * @package    Skos_Taxonomy_Plugin
 * @subpackage Skos_Taxonomy_Plugin/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Skos_Taxonomy_Plugin
 * @subpackage Skos_Taxonomy_Plugin/includes
 * @author     Sotirios Karampatakis <s.karampatakis@gmail.com>
 */
class Skos_Taxonomy_Plugin_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
