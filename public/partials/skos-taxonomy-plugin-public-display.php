<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://github.com/skarampatakis
 * @since      1.0.0
 *
 * @package    Skos_Taxonomy_Plugin
 * @subpackage Skos_Taxonomy_Plugin/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
