<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://github.com/skarampatakis
 * @since      1.0.0
 *
 * @package    Skos_Taxonomy_Plugin
 * @subpackage Skos_Taxonomy_Plugin/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap">
    <h2><?php echo esc_html(get_admin_page_title()); ?></h2>
    <button onclick='populate_skos()'>Populate SKOS Categories from ISCED 2011</button>
    <p id="skos-status" class="status-text"></p>
    <div class="skos spinner"></div>
</div>
