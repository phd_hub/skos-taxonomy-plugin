jQuery(function($) {
});

function populate_skos() {
	jQuery(".spinner").show();
	jQuery("#skos-status").html("Loading SKOS classifications...please wait....");
	jQuery.ajax({
		type: "POST",
		url: ajaxurl,
		data: {
			action: "populate_skos_categories",
		},
		success: function (data) {
			jQuery("#skos-status").html("SKOS taxonomy of ISCED 2011 loaded successfully!");
			jQuery(".spinner").hide();
		}
	});
}